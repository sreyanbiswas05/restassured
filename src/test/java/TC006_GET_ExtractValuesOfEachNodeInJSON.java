import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC006_GET_ExtractValuesOfEachNodeInJSON {
    @Test
    public void getWeatherDetails() {
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";

        //Request object
        RequestSpecification httpRequest = RestAssured.given();

        //Response object
        Response response = httpRequest.request(Method.GET, "/Delhi");

        JsonPath jsonpath = response.jsonPath();

        System.out.println(String.valueOf(jsonpath.get("City")));
        System.out.println(String.valueOf(jsonpath.get("Temperature")));
        System.out.println(String.valueOf(jsonpath.get("WindSpeed")));

        Assert.assertEquals(jsonpath.get("Temperature"),"39 degree celsius");
    }
}
