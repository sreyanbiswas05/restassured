package DataDrivenTesting;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class DataDrivenTest_AddNewEmployee {
    @Test(dataProvider = "empdataprovider")
    public void postNewEmployee(String ename,String eage,String esal){
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";
        RequestSpecification httpRequest = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("name",ename);
        requestParams.put("age",eage);
        requestParams.put("salary",esal);
        httpRequest.header("content-type","application/json");
        httpRequest.body(requestParams.toJSONString());
        Response response = httpRequest.request(Method.POST,"/create");
        String responseBody = response.getBody().asString();
        System.out.println(responseBody);
        Assert.assertEquals(responseBody.contains(ename),true);
        Assert.assertEquals(responseBody.contains(eage),true);
        Assert.assertEquals(responseBody.contains(esal),true);
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode,200);
    }
    @DataProvider(name="empdataprovider")
    Object[][] getEmployeeData() throws IOException {

        String path = "/Users/sreyanbiswas/Documents/SampleJava2/src/test/java/DataDrivenTesting/Book1.xlsx";

        int rownum = XLUtils.getRowCount(path,"Sheet1");
        int colcount = XLUtils.getCellCount(path,"Sheet1",1);

        String empdata[][] = new String[rownum][colcount];

        for(int i=1;i<=rownum;i++){
            for(int j=0;j<colcount;j++){
                empdata[i-1][j] = XLUtils.getCellData("Books1","Sheet1",i,j);
            }
        }

        //String data[][] = {{"sreyan","23","34567"},{"sdfrio","24","67451"},{"ascviasd","56","98675"}};
        return (empdata);
    }
}
